#!/usr/bin/python

__author__ = 'Alexander Sheredin'

from apiclient.discovery import build
from optparse import OptionParser
from collections import OrderedDict
import re

# Set DEVELOPER_KEY to the "API key" value from the "Access" tab of the
# Google APIs Console http://code.google.com/apis/console#access
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = "MY_KEY"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

#ROW NAMES
CHANNEL_NAME = "channel name"
CHANNEL_URL = "channel url"
DESCRIPTION = "about"
SUBSCRIBERS_CNT = "subscriber count"
EMAIL = "email address"
AVG_VIEW_COUNT = "average video count"
FEATURED = "featured channels"

MIN_SUBSCRIBER_CNT = 5000
EMAIL_REGEXP = re.compile(r'\w+@\w+')

youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                developerKey=DEVELOPER_KEY)

def channel_list(id):
    results = []
    response = youtube.channels().list(
        id=id,
        part="brandingSettings,statistics",
    ).execute()

    for result in response.get("items", []):
        results.append(result)

    print "Channel list result: %s" % results
    return results[0]


def youtube_search(options):
    search_response = youtube.search().list(
        q=options.q,
        part="id,snippet",
        order="viewCount",
        type="channel"
    ).execute()

    videos = []
    channels = []
    playlists = []
    channel_list = []

    for search_result in search_response.get("items", []):
        if search_result["id"]["kind"] == "youtube#video":
            videos.append("%s (%s)" % (search_result["snippet"]["title"],
                                       search_result["id"]["videoId"]))
        elif search_result["id"]["kind"] == "youtube#channel":
            channels.append("%s" % (search_result))
            channel_list.append(search_result)
        elif search_result["id"]["kind"] == "youtube#playlist":
            playlists.append("%s (%s)" % (search_result["snippet"]["title"],
                                          search_result["id"]["playlistId"]))

    print "Videos:\n", "\n".join(videos).encode('utf-8').strip(), "\n"
    print "Channels:\n", "\n".join(channels).encode('utf-8').strip(), "\n"
    print "Playlists:\n", "\n".join(playlists).encode('utf-8').strip(), "\n"

    return channel_list, videos

def get_avg_view_count(id):
    return ""

def process_channels(channels):
    def get_email(text):
        match = re.search(EMAIL_REGEXP, text)
        return match.group() if match else ""

    channel_dict = OrderedDict()
    featured_channels = set()

    for ch in channels:
        id = ch["id"]["channelId"]

        ch_list = channel_list(id)
        subscriber_cnt = ch_list["statistics"]["subscriberCount"]
        if subscriber_cnt < MIN_SUBSCRIBER_CNT:
            continue

        description = ch["snippet"]["description"]
        featured = set(ch_list["brandingSettings"].get("channel", {}).get("featuredChannelsUrls", []))
        featured_channels |= featured
        channel_dict[id] = {
            CHANNEL_URL: "http://www.youtube.com/channel/" + id,
            CHANNEL_NAME: ch["snippet"]["title"],
            DESCRIPTION: description,
            SUBSCRIBERS_CNT: subscriber_cnt,
            EMAIL: get_email(description),
            AVG_VIEW_COUNT: get_avg_view_count(id),
            FEATURED: featured
        }

    return channel_dict


def print_csv(filename, channels):
    import csv
    attr_list = [CHANNEL_NAME, CHANNEL_URL, DESCRIPTION, SUBSCRIBERS_CNT,
                 EMAIL, AVG_VIEW_COUNT, FEATURED]

    with open(filename, 'wb') as csvfile:
        csvfile.seek(0)
        writer = csv.writer(csvfile, delimiter=';')

        def encode1251(x):
            return unicode(x).encode('cp1251', 'ignore')

        writer.writerow(attr_list)
        for ch in channels.itervalues():
            row = [ch.get(attr, "") for attr in attr_list]
            writer.writerow(map(encode1251, row))


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("--q", dest="q", help="Search term",
                      default="Geek")
    parser.add_option("--max-results", dest="maxResults",
                      help="Max results", default=25)
    (options, args) = parser.parse_args()


    channels, videos = youtube_search(options)

    # process channel search results
    channels_ready = process_channels(channels)

    # print csv
    filename = options.q + ".csv"
    print_csv(filename, channels_ready)